package ru.t1.oskinea.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.api.repository.IProjectRepository;
import ru.t1.oskinea.tm.api.service.IProjectService;
import ru.t1.oskinea.tm.enumerated.Status;
import ru.t1.oskinea.tm.exception.entity.ProjectNotFoundException;
import ru.t1.oskinea.tm.exception.field.*;
import ru.t1.oskinea.tm.model.Project;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository>
        implements IProjectService {

    public ProjectService(final IProjectRepository repository) {
        super(repository);
    }

    @NotNull
    @Override
    public Project changeProjectStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @NotNull
    @Override
    public Project changeProjectStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();
        return repository.create(userId, name);
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();
        if (description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(userId, name, description);
    }

    @NotNull
    @Override
    public Project updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    public Project updateByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String name,
            @NotNull final String description
    ) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        if (name.isEmpty()) throw new NameEmptyException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

}
